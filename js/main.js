// Set the width and height of the canvas to match screen
$("#main-canvas, #background-canvas, #background-video, #grid-canvas").attr({
    "width": window.innerWidth,
    "height": window.innerHeight
});

// Create the digital clock widget
var clock_digital = new Digital_Clock("#digital-clock");
var clock_digital2 = new Digital_Clock_Simple("#digital-clock-simple");

// Create the analog clock widget
var clock_analog = new Analog_Clock("#analog-clock");
clock_analog.SetFacesFolderPath('modules/clock/release/img/faces/');

// Create the visuals with unique ID's
var visual_bar = new visualisations.bar.rainbow("bar_1");
var visual_bar2 = new visualisations.bar.rainbow("bar_2");

// Confetti visual
var confetti = new visualisations.background.confetti("confetti_1");

// Create the background module ( it's a visual )
var background = new visualisations.background.image("background_1");
background.settings.slideshowFolderProperty = "background_slideshowfolder";

// Create the main visualizer
var visualizer = $("#main-canvas").visualizer();
visualizer.setFPS(30);

// And the 'background-visualizer'
var background_engine = $("#background-canvas").visualizer();
background_engine.setFPS(30);

// Perspective module
var perspective = new Perspective("#main-canvas, clock");

// Grid
var grid = new modules.animatedGrid({selector : "#grid-canvas"});

// Add the visuals to the visualizer
background_engine.addVisual(background);
background_engine.addVisual(grid);

visualizer.addVisual(confetti);
visualizer.addVisual(visual_bar);
visualizer.addVisual(visual_bar2);

// Disable all modules by default
confetti.Enable(false);
background.Enable(false);
clock_digital.Enable(false);
clock_analog.Enable(false);
visual_bar.Enable(false);
visual_bar2.Enable(false);
perspective.Enable(false);
grid.Enable(false);

var performanceHelper = new PerformanceHelper();
performanceHelper.addVisualizer(visualizer);
performanceHelper.addVisualizer(background_engine);

// SOUND MAGIC STARTS HERE >>>>

// Listen to the sound Wallpaper Engine provides, or fallback to randomly generated data
if (window.wallpaperRegisterAudioListener) {
    window.wallpaperRegisterAudioListener(function(data) {
        visualizer.setAudioData(data);
        background_engine.setAudioData(data);
    });
} else {
    // When opened in a browser ( or if wallpaperRegisterAudioListener is not available, generate stub data )
    visualizer.generateFakeAudioData(true);
}