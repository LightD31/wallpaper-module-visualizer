var visualizerSettingsObject = {};
var visualizer_settings = function(settings) {

    // Set FPS if changed
    if (settings.fps) {
        visualizerSettingsObject.fps = settings.fps;
        visualizer.setFPS(settings.fps);
    }
    // Enable FPS counter
    if (settings.visualizer_fpscounter) {
        if (settings.visualizer_fpscounter.value !== "") {
            performanceHelper.Enable(settings.visualizer_fpscounter.value);
        }
    }
    // Set glow size
    if (settings.visualizer_glowsize) {
        if (settings.visualizer_glowsize.value !== "") {
            visualizer.settings.glowSize = settings.visualizer_glowsize.value;
            visualizer.enableGlow(visualizer.settings.enableGlow);
        }
    }
    // Set glow color
    if (settings.visualizer_glowcolor) {
        if (settings.visualizer_glowcolor.value !== "") {
            var c = settings.visualizer_glowcolor.value;
            var colour = new Colour().FromRGBOneScaleString(c);
            visualizer.settings.glowColor = colour.RGBColourString();
            visualizer.enableGlow(visualizer.settings.enableGlow);
        }
    }
    // Enable glow
    if (settings.visualizer_glow) {
        if (settings.visualizer_glow.value !== "") {
            visualizer.enableGlow(settings.visualizer_glow.value);
        }
    }
    // Enable idle state
    if (settings.visualizer_idlestate) {
        if (settings.visualizer_idlestate.value !== "") {
            visualizer.settings.enableIdleState = settings.visualizer_idlestate.value;
        }
    }
    // Set idle state timeout
    if (settings.visualizer_idletimeout) {
        if (settings.visualizer_idletimeout.value !== "") {
            visualizer.settings.idleTimeoutSeconds = settings.visualizer_idletimeout.value;
        }
    }
    // Set idle state movement speed
    if (settings.visualizer_idlemovementspeed) {
        if (settings.visualizer_idlemovementspeed.value !== "") {
            visualizer.settings.idleMovementSpeedMultiplier = settings.visualizer_idlemovementspeed.value / 100;
        }
    }
    // Set idle state strength
    if (settings.visualizer_idlestrength) {
        if (settings.visualizer_idlestrength.value !== "") {
            visualizer.settings.idleAnimationStrengthMultiplier = settings.visualizer_idlestrength.value / 100;
        }
    }
    // Set idle state ignores effects
    if (settings.visualizer_idleignoreeffects) {
        if (settings.visualizer_idleignoreeffects.value !== "") {
            visualizer.settings.idleAnimationIgnoresEffects = settings.visualizer_idleignoreeffects.value;
        }
    }

    // Effects
    if (settings.visualizer_effect) {
        if (settings.visualizer_effect.value !== "") {
            visualizer.settings.effectsEnabled = settings.visualizer_effect.value;
        }
    }
    if (settings.visualizer_effect_bluronbeat) {
        if (settings.visualizer_effect_bluronbeat.value !== "") {
            visualizer.settings.blurOnBeatEnabled = settings.visualizer_effect_bluronbeat.value;
        }
    }
    if (settings.visualizer_effect_bluronbeatstrength) {
        if (settings.visualizer_effect_bluronbeatstrength.value !== "") {
            visualizer.settings.blurOnBeatStrengthMultiplier = settings.visualizer_effect_bluronbeatstrength.value / 100;
        }
    }
    if (settings.visualizer_effect_hilightonbeat) {
        if (settings.visualizer_effect_hilightonbeat.value !== "") {
            visualizer.settings.hilightOnBeatEnabled = settings.visualizer_effect_hilightonbeat.value;
        }
    }
    if (settings.visualizer_effect_hilightonbeatstrength) {
        if (settings.visualizer_effect_hilightonbeatstrength.value !== "") {
            visualizer.settings.hilightOnBeatStrengthMultiplier = settings.visualizer_effect_hilightonbeatstrength.value / 100;
        }
    }
    if (settings.visualizer_effect_bounceonbeat) {
        if (settings.visualizer_effect_bounceonbeat.value !== "") {
            visualizer.settings.bounceOnBeatEnabled = settings.visualizer_effect_bounceonbeat.value;
        }
    }
    if (settings.visualizer_effect_bounceonbeatstrength) {
        if (settings.visualizer_effect_bounceonbeatstrength.value !== "") {
            visualizer.settings.bounceOnBeatStrengthMultiplier = settings.visualizer_effect_bounceonbeatstrength.value / 100;
        }
    }

    // Hue
    if (settings.visualizer_effect_hue) {
        if (settings.visualizer_effect_hue.value !== "") {
            visualizer.settings.hueEnabled = settings.visualizer_effect_hue.value;
        }
    }
    if (settings.visualizer_effect_huerotate) {
        if (settings.visualizer_effect_huerotate.value !== "") {
            visualizer.settings.hueOffset = settings.visualizer_effect_huerotate.value;
        }
    }
    if (settings.visualizer_effect_huerotation) {
        if (settings.visualizer_effect_huerotation.value !== "") {
            visualizer.startHueRotationTween(settings.visualizer_effect_huerotation.value);
        }
    }
    if (settings.visualizer_effect_huerotationspeed) {
        if (settings.visualizer_effect_huerotationspeed.value !== "") {
            visualizer.settings.hueRotationDuration = settings.visualizer_effect_huerotationspeed.value * 1000;
            visualizer.startHueRotationTween(visualizer.settings.hueRotationEnabled);
        }
    }
    if (settings.visualizer_effect_hueshifttemp) {
        if (settings.visualizer_effect_hueshifttemp.value !== "") {
            visualizer.settings.hueShiftTemporaryEnabled = settings.visualizer_effect_hueshifttemp.value;
        }
    }
    if (settings.visualizer_effect_hueshifttempstrength) {
        if (settings.visualizer_effect_hueshifttempstrength.value !== "") {
            visualizer.settings.hueShiftTemporaryStrengthMultiplier = settings.visualizer_effect_hueshifttempstrength.value / 100;
        }
    }
    if (settings.visualizer_effect_hueshiftperm) {
        if (settings.visualizer_effect_hueshiftperm.value !== "") {
            visualizer.settings.hueShiftPermanentEnabled = settings.visualizer_effect_hueshiftperm.value;
        }
    }
    if (settings.visualizer_effect_hueshiftpermstrength) {
        if (settings.visualizer_effect_hueshiftpermstrength.value !== "") {
            visualizer.settings.hueShiftPermanentStrengthMultiplier = settings.visualizer_effect_hueshiftpermstrength.value / 100;
        }
    }
    if (settings.visualizer_effect_hueshiftpermreset) {
        if (settings.visualizer_effect_hueshiftpermreset.value !== "") {
            visualizer.settings.hueShiftPermanentReset = settings.visualizer_effect_hueshiftpermreset.value;
        }
    }
    if (settings.visualizer_effect_hueshiftpermresettimeout) {
        if (settings.visualizer_effect_hueshiftpermresettimeout.value !== "") {
            visualizer.settings.hueShiftPermanentResetTimeout = settings.visualizer_effect_hueshiftpermresettimeout.value * 1000;
        }
    }
    if (settings.visualizer_effect_hueshiftpermresetduration) {
        if (settings.visualizer_effect_hueshiftpermresetduration.value !== "") {
            visualizer.settings.hueShiftPermanentResetDuration = settings.visualizer_effect_hueshiftpermresetduration.value * 1000;
        }
    }

    // Equalizer
    if (settings.visualizer_equalizer) {
        if (settings.visualizer_equalizer.value !== "") {
            visualizer.settings.enableEqualizer = settings.visualizer_equalizer.value;
        }
    }
    if (settings.visualizer_equalizer_1) {
        if (settings.visualizer_equalizer_1.value !== "") {
            visualizer.settings.equalizerMultiplier1 = settings.visualizer_equalizer_1.value / 100;
        }
    }
    if (settings.visualizer_equalizer_2) {
        if (settings.visualizer_equalizer_2.value !== "") {
            visualizer.settings.equalizerMultiplier2 = settings.visualizer_equalizer_2.value / 100;
        }
    }
    if (settings.visualizer_equalizer_3) {
        if (settings.visualizer_equalizer_3.value !== "") {
            visualizer.settings.equalizerMultiplier3 = settings.visualizer_equalizer_3.value / 100;
        }
    }
    if (settings.visualizer_equalizer_4) {
        if (settings.visualizer_equalizer_4.value !== "") {
            visualizer.settings.equalizerMultiplier4 = settings.visualizer_equalizer_4.value / 100;
        }
    }
    if (settings.visualizer_equalizer_5) {
        if (settings.visualizer_equalizer_5.value !== "") {
            visualizer.settings.equalizerMultiplier5 = settings.visualizer_equalizer_5.value / 100;
        }
    }
    if (settings.visualizer_equalizer_6) {
        if (settings.visualizer_equalizer_6.value !== "") {
            visualizer.settings.equalizerMultiplier6 = settings.visualizer_equalizer_6.value / 100;
        }
    }
    if (settings.visualizer_equalizer_7) {
        if (settings.visualizer_equalizer_7.value !== "") {
            visualizer.settings.equalizerMultiplier7 = settings.visualizer_equalizer_7.value / 100;
        }
    }
    if (settings.visualizer_equalizer_8) {
        if (settings.visualizer_equalizer_8.value !== "") {
            visualizer.settings.equalizerMultiplier8 = settings.visualizer_equalizer_8.value / 100;
        }
    }
}