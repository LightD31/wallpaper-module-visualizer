var confetti_settings = function(settings) {
   
    // Enable confetti
    if (settings.confetti_enable) {
        if (settings.confetti_enable.value !== "") {
            confetti.Enable(settings.confetti_enable.value)
        }
    }
    // Minimum sound to spawn confetti
    if (settings.confetti_minsound) {
        if (settings.confetti_minsound.value !== "") {
            confetti.settings.minimumSound = settings.confetti_minsound.value;
        }
    }
    // Set confetti flake size
    if (settings.confetti_size) {
        if (settings.confetti_size.value !== "") {
            confetti.settings.size = settings.confetti_size.value;
        }
    }
    // Set confetti spawn radius size
    if (settings.confetti_spawnradius) {
        if (settings.confetti_spawnradius.value !== "") {
            confetti.settings.spawnCircleSize = settings.confetti_spawnradius.value;
        }
    }
    // Set confetti burst spawn delay
    if (settings.confetti_spawndelay) {
        if (settings.confetti_spawndelay.value !== "") {
            confetti.settings.spawnDelay = settings.confetti_spawndelay.value;
        }
    }
    // Set confetti burst size
    if (settings.confetti_burstsize) {
        if (settings.confetti_burstsize.value !== "") {
            confetti.settings.burstSize = settings.confetti_burstsize.value;
        }
    }
    // Set confetti speed
    if (settings.confetti_animationspeed) {
        if (settings.confetti_animationspeed.value !== "") {
            confetti.settings.animationSpeed = settings.confetti_animationspeed.value / 10;
        }
    }
    // Set confetti color
    if (settings.confetti_customcolor) {
        if (settings.confetti_customcolor.value !== "") {
            var c = settings.confetti_customcolor.value;
            var colour = new Colour().FromRGBOneScaleString(c);
            confetti.settings.color = colour.RGBColourString();
        }
    }
    // Enable custom color
    if (settings.confetti_usecustomcolor) {
        if (settings.confetti_usecustomcolor.value !== "") {
            if (!settings.confetti_usecustomcolor.value) {
                confetti.settings.color = null;
            }
        }
    }
    // Offset X
    if (settings.confetti_offsetx) {
        if (settings.confetti_offsetx.value !== "") {
            confetti.settings.offsetX = settings.confetti_offsetx.value;
        }
    }
    // Offset Y
    if (settings.confetti_offsety) {
        if (settings.confetti_offsety.value !== "") {
            confetti.settings.offsetY = settings.confetti_offsety.value;
        }
    }
    // Enable confetti in visualizer idle mode
    if (settings.confetti_enablewhenidle) {
        if (settings.confetti_enablewhenidle.value !== "") {
            confetti.settings.enabledIdle = settings.confetti_enablewhenidle.value;
        }
    }
    // Enable confetti rotation
    if (settings.confetti_rotate) {
        if (settings.confetti_rotate.value !== "") {
            confetti.EnableRotation(settings.confetti_rotate.value)
        }
    }
    // Set confetti rotation speed
    if (settings.confetti_rotateduration) {
        if (settings.confetti_rotateduration.value !== "") {
            confetti.setRotationDuration(settings.confetti_rotateduration.value * 1000)
        }
    }
}