var visual_bar_settings = function(settings) {
    // Set enabled or disabled
    if (settings.visual_bar_enabled) {
        visual_bar.Enable(settings.visual_bar_enabled.value);
    }
    // Set number of bars
    if (settings.visual_bar_numberbars) {
        if (settings.visual_bar_numberbars.value !== "") {
            visual_bar.setPrecision(settings.visual_bar_numberbars.value);
            visual_bar.enableRotateColours(visual_bar.settings.enableRotateColours);
        }
    }
    // Set Channel
    if (settings.visual_bar_channel) {
        if (settings.visual_bar_channel.value !== "") {
            visual_bar.settings.channel = settings.visual_bar_channel.value;
        }
    }
    // Channel Render
    if (settings.visual_bar_channel_option) {
        if (settings.visual_bar_channel_option.value !== "") {
            visual_bar.settings.channel_render = settings.visual_bar_channel_option.value;
        }
    }
    // Channel Reverse
    if (settings.visual_bar_channel_mirror) {
        if (settings.visual_bar_channel_mirror.value !== "") {
            visual_bar.settings.channel_reverse = settings.visual_bar_channel_mirror.value;
        }
    }
    // Set the strength multiplier
    if (settings.visual_bar_strength) {
        if (settings.visual_bar_strength.value !== "") {
            visual_bar.settings.strengthMultiplier = settings.visual_bar_strength.value;
        }
    }
    // Set the bar width
    if (settings.visual_bar_barwidth) {
        if (settings.visual_bar_barwidth.value !== "") {
            visual_bar.settings.barWidth = settings.visual_bar_barwidth.value;
        }
    }
    // Set the bar gap
    if (settings.visual_bar_bargap) {
        if (settings.visual_bar_bargap.value !== "") {
            visual_bar.settings.barGap = settings.visual_bar_bargap.value;
        }
    }
    // Set custom color
    if (settings.visual_bar_color) {
        if (settings.visual_bar_color.value !== "") {
            var c = settings.visual_bar_color.value;
            var colour = new Colour().FromRGBOneScaleString(c);
            visual_bar.settings.color = colour.RGBColourString();
        }
    }
    // Use custom color
    if (settings.visual_bar_enablecolor) {
        if (settings.visual_bar_enablecolor.value !== "") {
            if (!settings.visual_bar_enablecolor.value) {
                visual_bar.settings.color = null;
            }
        }
    }
    // Set visual alpha
    if (settings.visual_bar_alpha) {
        if (settings.visual_bar_alpha.value !== "") {
            visual_bar.settings.alpha = settings.visual_bar_alpha.value / 100;
        }
    }
    // Set the bar vertical position offset
    if (settings.visual_bar_verticalposition) {
        if (settings.visual_bar_verticalposition.value !== "") {
            visual_bar.settings.barsVerticalPosition = settings.visual_bar_verticalposition.value;
        }
    }
    // Set the bar horizontal position offset
    if (settings.visual_bar_horizontalposition) {
        if (settings.visual_bar_horizontalposition.value !== "") {
            visual_bar.settings.barsHorizontalPosition = settings.visual_bar_horizontalposition.value;
        }
    }
    // Set the bar rotation
    if (settings.visual_bar_rotation) {
        if (settings.visual_bar_rotation.value !== "") {
            visual_bar.settings.rotation = settings.visual_bar_rotation.value;
        }
    }
    // Set the bar height limit
    if (settings.visual_bar_heightlimit) {
        if (settings.visual_bar_heightlimit.value !== "") {
            visual_bar.settings.heightLimit = settings.visual_bar_heightlimit.value;
        }
    }
    // Set the bar vertical growth offset
    if (settings.visual_bar_verticalgrowthoffset) {
        if (settings.visual_bar_verticalgrowthoffset.value !== "") {
            visual_bar.settings.verticalGrowthOffset = settings.visual_bar_verticalgrowthoffset.value;
        }
    }
    // Set the bar horitontal split
    if (settings.visual_bar_horizontalsplit) {
        if (settings.visual_bar_horizontalsplit.value !== "") {
            visual_bar.settings.enableSplit = settings.visual_bar_horizontalsplit.value;
        }
    }
    // Set the bar horizontal split alpha
    if (settings.visual_bar_horizontalsplitalpha) {
        if (settings.visual_bar_horizontalsplitalpha.value !== "") {
            visual_bar.settings.splitAlpha = settings.visual_bar_horizontalsplitalpha.value / 100;
        }
    }
    // Set the bar rotate colors
    if (settings.visual_bar_rotatecolor) {
        if (settings.visual_bar_rotatecolor.value !== "") {
            visual_bar.enableRotateColours(settings.visual_bar_rotatecolor.value);
        }
    }
    // Set the bar rotate color duration
    if (settings.visual_bar_rotatecolorspeed) {
        if (settings.visual_bar_rotatecolorspeed.value !== "") {
            visual_bar.settings.rotateColoursDuration = settings.visual_bar_rotatecolorspeed.value * 1000;
            visual_bar.enableRotateColours(visual_bar.settings.enableRotateColours);
        }
    }
    // Set the bar rotate colors left to right
    if (settings.visual_bar_rotatecolorl2r) {
        if (settings.visual_bar_rotatecolorl2r.value !== "") {
            visual_bar.settings.rotateColoursL2R = settings.visual_bar_rotatecolorl2r.value;
        }
    }
    // Enable circle mode
    if (settings.visual_bar_circlemode) {
        if (settings.visual_bar_circlemode.value !== "") {
            visual_bar.settings.circleMode = settings.visual_bar_circlemode.value;
        }
    }
    // Set the circle mode size
    if (settings.visual_bar_circlesize) {
        if (settings.visual_bar_circlesize.value !== "") {
            visual_bar.settings.circleSize = settings.visual_bar_circlesize.value;
        }
    }
    // Enable rotation
    if (settings.visual_bar_enablerotation) {
        if (settings.visual_bar_enablerotation.value !== "") {
            visual_bar.enableRotation(settings.visual_bar_enablerotation.value, visual_bar.settings.rotationCCW);
        }
    }
    // Enable rotation CCW
    if (settings.visual_bar_rotationccw) {
        if (settings.visual_bar_rotationccw.value !== "") {
            visual_bar.enableRotation(visual_bar.settings.enableRotation, settings.visual_bar_rotationccw.value);
        }
    }
    // Set rotation speed
    if (settings.visual_bar_rotationspeed) {
        if (settings.visual_bar_rotationspeed.value !== "") {
            visual_bar.settings.rotationDuration = settings.visual_bar_rotationspeed.value * 1000;
            visual_bar.enableRotation(visual_bar.settings.enableRotation, visual_bar.settings.rotationCCW);
        }
    }
    // Enable the border
    if (settings.visual_bar_enableborder) {
        if (settings.visual_bar_enableborder.value !== "") {
            visual_bar.settings.enableBorder = settings.visual_bar_enableborder.value;
        }
    }
    // Set the border width
    if (settings.visual_bar_borderwidth) {
        if (settings.visual_bar_borderwidth.value !== "") {
            visual_bar.settings.borderWidth = settings.visual_bar_borderwidth.value;
        }
    }
    // Set the border alpha
    if (settings.visual_bar_borderalpha) {
        if (settings.visual_bar_borderalpha.value !== "") {
            visual_bar.settings.borderAlpha = settings.visual_bar_borderalpha.value / 100;
        }
    }
    // Set the border color
    if (settings.visual_bar_bordercolor) {
        if (settings.visual_bar_bordercolor.value !== "") {
            var c = settings.visual_bar_bordercolor.value;
            var color = new Colour().FromRGBOneScaleString(c);
            visual_bar.settings.borderColor = color.RGBColourString();
        }
    }
    // Set the cap type of the lines
    if (settings.visual_bar_capstype) {
        if (settings.visual_bar_capstype.value !== "") {
            visual_bar.settings.lineCapsType = settings.visual_bar_capstype.value;
        }
    }
    // reverse rainbow colors
    if (settings.visual_bar_reverserainbow) {
        if (settings.visual_bar_reverserainbow.value !== "") {
            visual_bar.settings.reverseRainbow = settings.visual_bar_reverserainbow.value;
        }
    }
    // Hide inactive
    if (settings.visual_bar_hideinactive) {
        if (settings.visual_bar_hideinactive.value !== "") {
            visual_bar.settings.hideNoSound = settings.visual_bar_hideinactive.value;
        }
    }
    // Inactive wait time
    if (settings.visual_bar_hideinactivetimeout) {
        if (settings.visual_bar_hideinactivetimeout.value !== "") {
            visual_bar.settings.hideNoSoundDelay = settings.visual_bar_hideinactivetimeout.value * 1000;
        }
    }
    // Inactive fade duration
    if (settings.visual_bar_hideinactivefadeduration) {
        if (settings.visual_bar_hideinactivefadeduration.value !== "") {
            visual_bar.settings.hideNoSoundFadeDuration = settings.visual_bar_hideinactivefadeduration.value * 1000;
        }
    }
}