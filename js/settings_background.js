var backgroundSettingsObject = {};
var background_settings = function(settings) {
    // Select background type
    var setBackground = function() {

        var type = backgroundSettingsObject.type;

        if (!backgroundSettingsObject.enabled) {
            type = 99;
        }

        switch (type) {
            case 1:
            default:
                background.Enable(true);
                background.stopSlideShow(true);
                background.setImage(null, true);
                background.setVideo(null);
                document.body.style.backgroundColor = (type == 99) ? 'black' : backgroundSettingsObject.color;
                break;
            case 2:
                background.Enable(true);
                background.setVideo(null);
                background.stopSlideShow(true);
                background.setImage(backgroundSettingsObject.image, true);
                document.body.style.backgroundColor = 'black';
                break;
            case 3:
                background.Enable(true);
                background.setVideo(null);
                background.startSlideShow();
                document.body.style.backgroundColor = 'black';
                break;
            case 4:
                background.Enable(true);
                background.stopSlideShow(true);
                background.setImage(null, true);
                background.setVideo(backgroundSettingsObject.video);
                document.body.style.backgroundColor = 'black';
                break;
        }
    }

    // Enable/Disable background
    if (settings.background_enable) {
        if (settings.background_enable.value !== "") {
            if (!settings.background_enable.value) {
                backgroundSettingsObject.enabled = false;
            } else {
                backgroundSettingsObject.enabled = true;
            }
            setBackground();
        }
    }

    if (settings.background_type) {
        if (settings.background_type.value !== "") {
            backgroundSettingsObject.type = settings.background_type.value;

            setBackground();
        }
    }
    // Select background color
    if (settings.background_color) {
        if (settings.background_color.value !== "") {
            var c = settings.background_color.value;
            var colour = new Colour().FromRGBOneScaleString(c);
            backgroundSettingsObject.color = colour.RGBColourString();

            setBackground();
        }
    }
    // Select background image
    if (settings.background_image) {
        if (settings.background_image.value !== "") {
            backgroundSettingsObject.image = "file:///" + settings.background_image.value;
        } else {
            backgroundSettingsObject.image = null;
        }
        setBackground();
    }
    // Set Slideshow image show duration
    if (settings.background_slideshowduration) {
        if (settings.background_slideshowduration.value !== "") {
            background.settings.slideShowShowTime = settings.background_slideshowduration.value * 1000;
        }
    }
    // Set Slideshow fade duration
    if (settings.background_slideshowfadeduration) {
        if (settings.background_slideshowfadeduration.value !== "") {
            background.settings.slideShowFadeDuration = settings.background_slideshowfadeduration.value * 1000;
        }
    }
    // Select background video
    if (settings.background_videofile) {
        if (settings.background_videofile.value !== "") {
            backgroundSettingsObject.video = "file:///" + settings.background_videofile.value;
        } else {
            backgroundSettingsObject.video = null;
        }
        setBackground();
    }
    // Video playback speed 
    if (settings.background_videovolume) {
        if (settings.background_videovolume.value !== "") {
            var vid = document.getElementById("background-video");
            vid.volume = settings.background_videovolume.value / 100;
        }
    }
    // Video volume
    if (settings.background_videospeed) {
        if (settings.background_videospeed.value !== "") {
            var vid = document.getElementById("background-video");
            vid.playbackRate = settings.background_videospeed.value / 100;
        }
    }
    // Video pause when idle
    if (settings.background_idlepause) {
        if (settings.background_idlepause.value !== "") {
            background.settings.pauseVideoOnIdle = settings.background_idlepause.value;
        }
    }

    // Scale background to fit
    if (settings.background_scaletofit) {
        if (settings.background_scaletofit.value !== "") {
            background.settings.scaleBackgroundToFit = settings.background_scaletofit.value;
        }
    }

    // EFFECTS

    // Enable effects
    if (settings.background_effect) {
        if (settings.background_effect.value !== "") {
            background.settings.effectsEnabled = settings.background_effect.value;
        }
    }
    // Hue rotate
    if (settings.background_huerotate) {
        if (settings.background_huerotate.value !== "") {
            background.settings.hueRotate = settings.background_huerotate.value;
        }
    }

    // More hue
    if (settings.background_effect_hueshift) {
        if (settings.background_effect_hueshift.value !== "") {
            background.settings.hueShiftBassEnabled = settings.background_effect_hueshift.value;
        }
    }
    if (settings.background_effect_hueshiftstrength) {
        if (settings.background_effect_hueshiftstrength.value !== "") {
            background.settings.hueShiftBassStrengthMultiplier = settings.background_effect_hueshiftstrength.value / 100;
        }
    }

    if (settings.background_effect_huerotation) {
        if (settings.background_effect_huerotation.value !== "") {
            background.startHueRotationTween(settings.background_effect_huerotation.value);
        }
    }

    if (settings.background_effect_huerotationduration) {
        if (settings.background_effect_huerotationduration.value !== "") {
            background.settings.hueRotationDuration = settings.background_effect_huerotationduration.value * 1000;
            background.startHueRotationTween(background.settings.hueRotationEnabled);
        }
    }

    if (settings.background_effect_huerotateenabled) {
        if (settings.background_effect_huerotateenabled.value !== "") {
            background.settings.hueRotateEnabled = settings.background_effect_huerotateenabled.value;
        }
    }

    if (settings.background_effect_huerotatestrength) {
        if (settings.background_effect_huerotatestrength.value !== "") {
            background.settings.hueRotateStrengthMultiplier = settings.background_effect_huerotatestrength.value / 100;
        }
    }

    // Enable Blur
    if (settings.background_effect_blur) {
        if (settings.background_effect_blur.value !== "") {
            background.settings.blurEnabled = settings.background_effect_blur.value;
        }
    }
    // Set Blur Strength
    if (settings.background_effect_blurstrength) {
        if (settings.background_effect_blurstrength.value !== "") {
            background.settings.blurStrengthMultiplier = settings.background_effect_blurstrength.value / 100;
        }
    }
    // Enable Bounce
    if (settings.background_effect_bounce) {
        if (settings.background_effect_bounce.value !== "") {
            background.settings.scaleEnabled = settings.background_effect_bounce.value;
        }
    }
    // Set Bounce Strength
    if (settings.background_effect_bouncestrength) {
        if (settings.background_effect_bouncestrength.value !== "") {
            background.settings.scaleStrengthMultiplier = settings.background_effect_bouncestrength.value / 100;
        }
    }
    // Enable Hilight
    if (settings.background_effect_hilight) {
        if (settings.background_effect_hilight.value !== "") {
            background.settings.lightEnabled = settings.background_effect_hilight.value;
        }
    }
    // Set Hilight Strength
    if (settings.background_effect_hilightstrength) {
        if (settings.background_effect_hilightstrength.value !== "") {
            background.settings.lightStrengthMultiplier = settings.background_effect_hilightstrength.value / 100;
        }
    }

    if (settings.background_effect_hueshiftpermreset) {
        if (settings.background_effect_hueshiftpermreset.value !== "") {
            background.settings.hueShiftBassReset = settings.background_effect_hueshiftpermreset.value / 100;
        }
    }
    if (settings.background_effect_hueshiftpermresettimeout) {
        if (settings.background_effect_hueshiftpermresettimeout.value !== "") {
            background.settings.hueShiftBassResetTimeout = settings.background_effect_hueshiftpermresettimeout.value * 1000;
        }
    }
    if (settings.background_effect_hueshiftpermresetduration) {
        if (settings.background_effect_hueshiftpermresetduration.value !== "") {
            background.settings.hueShiftBassResetDuration = settings.background_effect_hueshiftpermresetduration.value * 1000;
        }
    }

    // Rocking settings
    if (settings.background_effect_rocking) {
        background.settings.rotationEnabled = settings.background_effect_rocking.value;
    }

    if (settings.background_effect_rockingstrength) {
        if (settings.background_effect_rockingstrength.value) {
            background.settings.rotationStrengthMultiplier = settings.background_effect_rockingstrength.value / 100;
        }
    }

    if (settings.background_effect_rockingspeed) {
        if (settings.background_effect_rockingspeed.value) {
            background.setRotationSpeed(settings.background_effect_rockingspeed.value / 100);
        }
    }
}