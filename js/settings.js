window.wallpaperPropertyListener = {

    setPaused: function(isPaused) {

        var video = $("#background-video");
        video.paused = isPaused;

        background_engine.Enable(!isPaused);
        visualizer.Enable(!isPaused);
    },

    applyGeneralProperties: function (properties) {
        
        // Set visualizer settings
        visualizer_settings(properties);
    },

    applyUserProperties: function(properties) {

        // Set visualizer settings
        visualizer_settings(properties);

        // Set the rainbowbars settings
        visual_bar_settings(properties);
        visual_bar2_settings(properties);

        // Set perspective settings
        perspective_settings(properties);

        // Set clock settings
        clock_settings(properties);

        // Set background settings
        background_settings(properties);

        // Set confetti settings
        confetti_settings(properties);

        // Background Grid settings
        grid_settings(properties);
    }
}